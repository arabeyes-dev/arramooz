#!/usr/bin/python
# -*- coding=utf-8 -*-
from ar_nouns import *
from ar_ctype import *
text=u"""عامل
سلام
نعمة
سمراء
قاضي
كتاب"""

wordlist=text.split("\n");
counter=0;
counter_generated=1;
for word in wordlist:
	unvoc_words=[];
	list0=generate_allforms(word, has_pronouns=True,has_jonction=True,has_preposition=True,has_definition=True,has_interrog=True,has_conjugation=True,has_qasam=True,is_defined=True);
	for l in list0:
		counter_generated+=1;
		stemmed=l;
		standard=standardize_form(l);
		line=u'\t\t'.join([standard, stemmed])
		unvoc_words.append(ar_strip_marks(standard));
		print line.encode('utf8');
	unvoc_words=list(set(unvoc_words));
	print "---------------------";
	for u in unvoc_words:
		print u.encode('utf8');
	print "---------------------";

