#!/usr/bin/python
# -*- coding=utf-8 -*-
#
import re, string,sys
from arabic_const import *
from ar_ctype import *

def chomp(s):
  if (s.endswith('\n')):
    return s[:-1]
  else:
    return s

#-----------------------------------------------
#
#
#-----------------------------------------------
pronouns=(
YEH,
HEH,
);
tanwin_suffix={
u'مرفوع':(DAMMATAN,),
u'منصوب':(FATHATAN+ALEF,),
u'مجرور':(KASRATAN,),
}
dual_suffix={
u'مرفوع':(FATHA+ALEF+NOON,ALEF),
u'منصوب':(FATHA+YEH,FATHA+YEH+NOON),
u'مجرور':(FATHA+YEH,FATHA+YEH+NOON),
}
femininplural_suffix={
u'مرفوع':(FATHA+ALEF+TEH,),
u'منصوب':(FATHA+ALEF+TEH,),
u'مجرور':(FATHA+ALEF+TEH,),
}
masculinplural_suffix={
u'مرفوع':(DAMMA+WAW+NOON,),
u'منصوب':(KASRA+YEH+NOON,),
u'مجرور':(KASRA+YEH+NOON,),
}


def generate_allforms(word,has_pronouns=False,has_jonction=False,has_preposition=False,has_definition=False,has_interrog=False,has_conjugation=False,has_qasam=False,is_defined=False):

# tofeminin
	# يقبل التأنيث يؤنث
	femininable=True;
	# مؤنث
	feminin=True;		
	# يقبل النسبة ينسب
	relativable=True
	# منسوب
	relative=False;
	tanwinable=True;
	dualable=True;
	dual=False;
	femininpluralable=True;
	masculinpluralable=True;
	femininplural=False;
	masculinplural=False;
	brokenplural=False;

	#الاشتقاق: التأنيث والنسبة، والمصدر الصناعي
# derivation section
	# add yeh for relative nouns
	mylist=[word];
	tags={};
	tags[word]=(u'مرفوع',);
	temp=[];
	# النسبة بالياء، تعالج لاحقا
	if relativable :
		for w in mylist:
			suffix=KASRA+YEH;
			newword="-".join([w,suffix]);
			temp.append(newword);
			tags[newword]=(u'مرفوع',);
	# add Teh Marbuta 
	mylist+=temp;
	temp=[];
	if femininable :
		for w in mylist:
			suffix=FATHA+TEH_MARBUTA;
			newword="-".join([w,suffix]);
			temp.append(newword);
			tags[newword]=(u'مرفوع',u'تاء مربوطة',);
	mylist+=temp;
	temp=[];
# conjugation section
	#i إعراب الكلمات، نصبا وجرا
	for w in mylist:

		suffix=FATHA;
		newword="-".join([w,suffix]);
		temp.append(newword);
		tags[newword]=(u'منصوب',);
		suffix=KASRA;
		newword="-".join([w,suffix]);
		temp.append(newword);
		tags[newword]=(u'مجرور',);	
	# dual affixation
#		# single
		
	mylist+=temp;
	temp=[];

	if tanwinable:
		for w in mylist:
			for suffix  in tanwin_suffix[tags[w][0]]:
				newword="-".join([w,suffix]);
				temp.append(newword);
				tags[newword]=tags[w]+(u'منون',);	
	# الفمرد المضاف :

	
	if dualable :
		for w in mylist:
			for suffix  in dual_suffix[tags[w][0]]:
				newword="-".join([w,suffix]);
				temp.append(newword);
				tags[newword]=tags[w];
				if 	 suffix in (ALEF,YEH):
					tags[newword]+=(u'مضاف',);					
	if femininpluralable :
		for w in mylist:
			for suffix  in femininplural_suffix[tags[w][0]]:
				newword="-".join([w,suffix]);
				temp.append(newword);
				tags[newword]=tags[w];
	if masculinpluralable :
		for w in mylist:
			if not w.endswith(TEH_MARBUTA):
				for suffix  in masculinplural_suffix[tags[w][0]]:
					newword="-".join([w,suffix]);
					temp.append(newword);
					tags[newword]=tags[w];
					if 	 suffix in (WAW,YEH):
						tags[newword]+=(u'مضاف',);
	for w in mylist:
		tags[w]+=(u'مضاف',);
		

# affixatition section				
	mylist+=temp;
	temp=[];
	has_pronouns=False;
	if has_pronouns:
		for w in mylist:
			if u'مضاف' in tags[w]:
				for p in pronouns:
					newword="-".join([w,p]);
					temp.append(newword);
					tags[newword]=tags[w]+(u'مضاف إليه',);
		
	if has_definition:
		for w in mylist:
			prefix=ALEF+LAM
			if u'منون' not in tags[w]:
					newword="-".join([prefix,w]);
					temp.append(newword);
					tags[newword]=tags[w]+(u'معرف',);

				
	mylist+=temp;
	return mylist;


def standardize_form(word,vocalized=False):
	if vocalized:
		word=re.sub(u"%s-"%FATHA,'',word)
		word=re.sub(u"%s-"%DAMMA,'',word)
		word=re.sub(u"%s-"%KASRA,'',word)
		word=re.sub(u"-%s"%FATHATAN,FATHATAN,word)
		word=re.sub(u"-%s"%DAMMATAN,DAMMATAN,word)
		word=re.sub(u"-%s"%KASRATAN,KASRATAN,word)
		word=re.sub(u"%s-%s"%(ALEF_MAKSURA,YEH),YEH,word)
		word=re.sub(u"%s-"%ALEF_MAKSURA,YEH,word)	
		word=re.sub(u"%s-%s"%(TEH_MARBUTA,FATHA+ALEF+TEH),ALEF+TEH,word)
		word=re.sub(u"%s-"%TEH_MARBUTA,TEH,word)

		word=re.sub(u"-",'', word);
	else:
		word=ar_strip_marks(word);
		word=re.sub(u"--",'-', word);
		word=re.sub(u"-$",'', word);
		word=re.sub(u"%s-%s"%(ALEF_MAKSURA,YEH),YEH,word)
		word=re.sub(u"%s-"%ALEF_MAKSURA,YEH,word)	
		word=re.sub(u"%s-%s"%(TEH_MARBUTA,ALEF+TEH),ALEF+TEH,word)
		word=re.sub(u"%s-"%TEH_MARBUTA,TEH,word)
		word=re.sub(u"-",'', word);
		word=re.sub(u"%s%s"%(TEH_MARBUTA,ALEF+TEH),ALEF+TEH,word)
	return word;
