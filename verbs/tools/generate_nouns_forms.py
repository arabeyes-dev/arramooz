#!/usr/bin/python
# -*- coding=utf-8 -*-
##from analex import *

import sys
import re
import  getopt
import os
import string
from ar_ctype import *
from ar_nouns import *
scriptname = "convert"
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2007, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\t[-o | --out= output format]\toutput format(csv,python,sql)"
	
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	fname = ''
	outputformat = 'csv'

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hV:f:o:",
                               ["help", "version",
                                 "file=", "out="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-f", "--file"):
			fname = val
		if o in ("-o", "--out"):
			outputformat = val;
		else:
			outputformat = 'csv';	
	return (fname,outputformat)

def main():

	filename,outputformat=grabargs()

	outputformat = string.lower(outputformat)
	if outputformat not in ('csv','python','sql'):
		outputformat='csv';
	print "# generated format",outputformat
	print "#file name ",filename
	if outputformat=='python':
		print "STOPWORDS={}";
	elif outputformat=='sql':
		print u'''create TABLE STOPWORDS
			(
			ID INT UNIQUE NOT NULL,
			WORD TEXT NOT NULL,
			STEMMEDWORD TEXT NOT NULL
			)
		''';


#ToDo1 ref
	if (not filename):
		usage()
		sys.exit(0)
	option="";
	try:
		fl=open(filename);
	except :
		print " Error :No such file or directory: %s" % filename
		return None;
	line=fl.readline().decode("utf8");
	text=u""
	limit=1000;
	counter=0;
	counter_generated=1;

	while line and counter<limit:
##		text=" ".join([text,chomp(line)])
		line=fl.readline().decode("utf8");
		line=chomp(line);
		if not line.startswith("#"):
			listword=line.split(";");
			if len(listword)>=9:
				word=ar_strip_marks(listword[0].strip());
				type_word=listword[1];
				class_word=listword[2];
				has_conjuction=listword[3];
				has_definition=listword[4];
				has_preposition=listword[5];
				has_pronoun=listword[6];
				has_interrog=listword[7];
				has_conjugation=listword[8];				
				has_qasam=listword[9];
				is_defined=listword[10];
				if has_conjuction=="*":
					has_conjuction=False;
				else:
					has_conjuction=True;
				if has_definition=="*":
					has_definition=False;
				else:
					has_definition=True;

				if has_preposition=="*":
					has_preposition=False;
				else:
					has_preposition=True;

				if has_pronoun=="*":
					has_pronoun=False;
				else:
					has_pronoun=True;
				if has_interrog=="*":
					has_interrog=False;
				else:
					has_interrog=True;
				if has_conjugation=="*":
					has_conjugation=False;
				else:
					has_conjugation=True;					
				if has_qasam==u"ل":
					has_qasam=True;
				else:
					has_qasam=False;
				if is_defined==u"ف":
					is_defined=True;
				else:
					is_defined=False;					  
				list0=generate_allforms(word,has_pronoun,has_conjuction,has_preposition,has_definition,has_interrog,has_conjugation, has_qasam,is_defined);
				for l in list0:
					counter_generated+=1;
					standard=standardize_form(l);
					stemmed=l;
					if outputformat=='sql':
						line=u"insert into STOPWORDS values ("+str(counter_generated)+",'"+standard+"', '"+stemmed+"');";
					elif outputformat=='python':
						line= u"STOPWORDS[u'"+standard+"']=u'"+stemmed+"';"
					else:
						line=u'\t'.join([standard, stemmed])
					print line.encode('utf8');
		counter+=1
if __name__ == "__main__":
  main()







