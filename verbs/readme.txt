#-----------
# Verb dictionary generation for Arramooz
#-------------
Arramooz is an arabic open source dictionary, built from ayaspell project,
its special for morphology analysis.


# FILES
========
* data/ contains the brut data from the ayaspell data
* core/ special functions
* libqutrub : used to generate all verbs conjugated forms, and convert it to hunspellor aspell dictionary
* generateverbdict.py : the main sciprt, usage is descripted in USAGE section.
* genverbdict_sql.bat : a shell script to create a sql format of the dictionary
* genverbdict_txt.bat : a shell script to create a txt format of the dictionary
* genverbdict_xml.bat : a shell script to create a xml format of the dictionary
* genverbdict_py.bat : a shell script to create a python format of the dictionary

# USAGE
========
generateverbdict.py
(C) CopyLeft 2009, Taha Zerrouki
Usage: generateverbdict -f filename [OPTIONS]
        [-h | --help]           outputs this usage message
        [-v | --version]        program version
        [-f | --file= filename] input file to generateverbdict
        [-d | --display= format]        display format (txt,sql, python, xml) ge
nerateverbdict
        [-t | --type= wordtype] give the word type(fa3il,masdar, jamid, mochabah
a,moubalagha,mansoub,) generateverbdict
        [-l | --limit= limit_ number]   the limit of treated lines generateverbd
ict


This program is licensed under the GPL License
