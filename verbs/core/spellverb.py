#!/usr/bin/python
# -*- coding=utf-8 -*-
#************************************************************************
# $Id: spellverb.py,v 0.7 2010/12/26 01:10:00 Taha Zerrouki $
#
# ------------
# Description:
# ------------
#  Copyright (c) 2009, Arabtechies, Arabeyes Taha Zerrouki
#
#  This file is the main file to execute the application in the command line
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date: 2009/06/02 01:10:00 $
#  $Author: Taha Zerrouki $
#  $Revision: 0.7 $
#  $Source: arabtechies.sourceforge.net
#
#***********************************************************************/

from spellverbconst import *
import pyarabic.araby as araby
def reduceEntries(word_nm, flags, verb_nm, pronoun, tense):
	"""
	
	Dictionary entries reduction
	To reduce the number of entries in the dictionationary
	we propose to stip NOON letters from the word and add it to the suffixes
	every word with a noon in conjugation suffix will be striped from the word, 
	and add if to the spelling suffix
	to add NOON to Spelling suffix we replace suffixe rule name started by H, by G.
	for example ضربن/HaHb => ضرب/GaGb
	
	@param word_nm: unvocalized word, conjugation form of the verb
	@type word_nm: unicode;
	@param flags: a string of Hunspell Flags
	@type flags: unicode.
	@param pronoun: the given pronoun for the given conjugation
	@type pronoun: unicode.
	@param tense: the given tense for the given conjugation
	@type tense: unicode.	
	@return: return the new word and  new flags after conversion.
	@rtype: (newword_nm, newflags);
	"""
	suffix=u"";
	if TableTensePronoun.has_key(tense):
		if  TableTensePronoun[tense].has_key(pronoun):
			if len(TableTensePronoun[tense][pronoun])>1:
				suffix=TableTensePronoun[tense][pronoun][1];
				suffix=araby.stripTashkeel(suffix);
	lastLetter=verb_nm[-1:];
	if suffix!="" and lastLetter not in (WAW, YEH, ALEF, ALEF_MAKSURA, ALEF_HAMZA_ABOVE,YEH_HAMZA, WAW_HAMZA):
		# change all words with noon as conjugation suffix by a new suffixe rule started with G
		# except WAW+NOON يضربون to avoid generation of invalid stem يضربو
		if not( suffix.startswith(NOON) and verb_nm.endswith(NOON)) and not (suffix.startswith(TEH) and verb_nm.endswith(TEH)):
			if TableSuffixFlag.has_key(suffix):
				newFlag=TableSuffixFlag[suffix];
				lenSuff=len(suffix);
				flags=flags.replace('H',newFlag);
				flags=flags.replace('G',newFlag);
				word_nm=word_nm[:-lenSuff];
				# إضافة علم خاص لتوليد حالة الفعل مع ال،ون دون ضمائر متصلة
				flags+='%sx'%newFlag;
	else:
		# change all words with noon as conjugation suffix by a new suffixe rule started with G
		# except WAW+NOON يضربون to avoid generation of invalid stem يضربو
		if suffix in (TEH+NOON, YEH+NOON, ALEF+NOON):#suffix.endswith(NOON) and not suffix.endswith(WAW+NOON) and not suffix.endswith(NOON+ALEF+NOON):
			flags=flags.replace('H','C');
			flags=flags.replace('G','C');			
			word_nm=word_nm[:-1];
			# إضافة علم خاص لتوليد حالة الفعل مع ال،ون دون ضمائر متصلة
			flags+='Gx';
		#التصريف مع ضمير المخاطب أنتم
		# استبداله بقزاعد إلحاقية تضم الميم في أول القاعدة
		# حذف الميم من آخر الكلمة
		elif suffix.endswith(TEH+MEEM):
			flags=flags.replace('H','I');
			flags=flags.replace('G','I');						
			word_nm=word_nm[:-1];

			# إضافة علم خاص لتوليد حالة الفعل مع الميم دون ضمائر متصلة		
			flags+='Ix';		
		#التصريف مع ضمير المخاطب أنتم
		# استبداله بقزاعد إلحاقية تضم الميم والألف في أول القاعدة
		# حذف الميم والألف من آخر الكلمة
		elif suffix.endswith(TEH+MEEM+ALEF):
			flags=flags.replace('H','J');
			word_nm=word_nm[:-2];
			# إضافة علم خاص لتوليد حالة الفعل مع الميم والألف دون ضمائر متصلة
			flags+='Jx';		
	return (word_nm, flags);
	

# table of suffixes of double transitive verbs
#جدةل لواحق الفعل  القلبي المتعدي لمغعول به عاقل، # used to eliminate duplicated flags

def expand_flags(tags):
	s=tags;
	count=2;
	listflag=[''.join(x) for x in zip(*[list(s[z::count]) for z in range(count)])]
	listflag=list(set(listflag));
	listflag.sort();
	return listflag;
# table of suffixes of double transitive verbs
#جدةل لواحق الفعل  القلبي المتعدي لمغعول به عاقل، # used to eliminate duplicated flags
def unify_flags(tags):
	listflag=expand_flags(tags)
	return ''.join(listflag);
def decode_tenses(field):
	all=False;
	past=False;
	future=False;
	passive=False;
	imperative=False;
	future_moode=False;
	confirmed=False;
	if field==u"يعملان":
		all=True;
	else:
		if field.find(YEH)>=0:
			past=True;
		if field.find(AIN)>=0:
			future=True;
		if field.find(MEEM)>=0:
			imperative=True;
		if field.find(LAM)>=0:
			passive=True;
		if field.find(ALEF)>=0:
			future_moode=True;
		if field.find(NOON)>=0:
			confirmed=True;
	return (all, past, future, passive, imperative, future_moode, confirmed);
	

def convert_flags_aspell(tags):
	"""
	after creation of hunspell flags for every word,
	we can convert verb affixes to Aspell
	The aspell does'nt support 2 letters affixes like Ha, Pa
	In verb affixes:
		prefixes starts by P, like Pa,Pb
		suffixes starts by H, Ha, Hb.
	in conversion:
		all prefixes are capitals, then Pa=>A
		all suffixes are lower cases the Ha=>a
	@parameters are the flags of the word
	@return: aspell format flags
	"""
	listflag=expand_flags(tags)
	newlist=[];
	for tag in listflag:
		if tag.startswith("H"):
			newlist.append(tag[1].lower())
		elif tag.startswith("P"):
			newlist.append(tag[1].upper());
		else:
			print "error tag=", tag;
	return ''.join(newlist);
	
def getMorph(verb_nm, pronoun, tense):
	"""
	Create morhp fields for hunspell
	@param verb_nm: the unconjugated unvocalized given verb
	@type verb_nm: unicode.	
	@param pronoun: the given pronoun for the given conjugation
	@type pronoun: unicode.
	@param tense: the given tense for the given conjugation
	@type tense: unicode.	
	@return: return the morphological fields.
	@rtype: unicode;
	"""
	morph=u"po:verb;st:%s;is:%s"%(verb_nm, TableTenseMorphCode.get(tense,'conj'))
	#To do add tense and pronoun
	#morph=u"po:verb st:%s is:%s"%(verb_nm, TableTenseMorphCode.get(tense,'conj'))
	
	return morph; 
	
def unify_morph(tags):
	listmorph=list(set(tags.split(';')));
	return ' '.join(listmorph);
