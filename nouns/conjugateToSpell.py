#!/usr/bin/python
# -*- coding=utf-8 -*-
#************************************************************************
# $Id: nountodict.py,v 0.7 2009/06/02 01:10:00 Taha Zerrouki $
#
# ------------
# Description: Convert a noun dict from Arramooz Fromat to Hunspell format
# ------------
#  Copyright (c) 2009, Arabtechies, Arabeyes Taha Zerrouki
#
#  This file is the main file to execute the application in the command line
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date: 2009/06/02 01:10:00 $
#  $Author: Taha Zerrouki $
#  $Revision: 0.7 $
#  $Source: arabtechies.sourceforge.net
#
#***********************************************************************/



import sys,re,string
import sys, getopt, os
from core.spelltools import *
from core.spellnounconst import *
from core.ar_nouns import *
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
MAX_LINES_TREATED=1100000;

# translate affixes from arramooz format to hunspell format
def translate_noun_affixes(noun_dict):
	affixes=u"";
	for key in noun_dict.values():
		#print "1";
		key=key.strip();
		if TRANSLATE_AFFIXES.has_key(key):
			affixes+=TRANSLATE_AFFIXES[key];
	affixes=unify_flags(affixes);
	return affixes;


def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-v | --version]\tprogram version"
	print "\t[-a | --aspell]\tcreate dictionary for aspell, by default hunspell format"	
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\t[-l | --limit= limit_ number]\tthe limit of treated lines %s"%scriptname
	print "\r\nN.B. FILE FORMAT is descripted in README"
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	fname = ''
	limit=MAX_LINES_TREATED;
	spellchecker = "hunspell"
	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hav:f:l:",
                               ["help", "aspell", "version", "file=","limit="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-v", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-a", "--aspell"):
			spellchecker = "aspell";
		if o in ("-f", "--file"):
			fname = val
		if o in ("-l", "--limit"):
			try:
				limit = int(val);
				if limit==0: limit=MAX_LINES_TREATED;
			except:
				limit=MAX_LINES_TREATED;

	print fname,limit,spellchecker;
	return fname,limit,spellchecker


                 
def main():
	filename,limit,spellchecker= grabargs()
	try:
		fl=open(filename);
	except:
		print " Error :No such file or directory: %s" % filename
		sys.exit(0)
	print "#",filename;


	line=fl.readline().decode("utf");
	text=u""
	noun_table=[];
	nb_field=2;
	while line :
		line=chomp(line)
		if not line.startswith("#"):
			liste=line.split("\t");
			if len(liste)>=nb_field:
				noun_table.append(liste);

		line=fl.readline().decode("utf8");
	fl.close();

# vocalized	unvocalized	wordtype	root	original	mankous	feminable	number	dualable	masculin_plural	feminin_plural	broken_plural	mamnou3_sarf	relative	w_suffix	hm_suffix	kal_prefix	ha_suffix	k_suffix	annex	definition	note

	model=0;
	for tuple_noun in noun_table[:limit]:
		noun_dict={};
		noun_dict['vocalized'] 		= tuple_noun[0];
		noun_dict['unvocalized']  = tuple_noun[1];
		noun_dict['wordtype']  		= tuple_noun[2];
		#noun_dict['root']  				= tuple_noun[3];
		#noun_dict['original']  		= tuple_noun[4];
		noun_dict['mankous']  		= tuple_noun[5];
		noun_dict['feminable']  	= tuple_noun[6];
		noun_dict['number']  			= tuple_noun[7];
		noun_dict['dualable']  		= tuple_noun[8];
		noun_dict['masculin_plural']= tuple_noun[9];
		noun_dict['feminin_plural'] = tuple_noun[10];
		noun_dict['broken_plural'] 	= tuple_noun[11];
		noun_dict['mamnou3_sarf']  	= tuple_noun[12];
		noun_dict['relative']  			= tuple_noun[13];
		noun_dict['w_suffix']  			= tuple_noun[14];
		noun_dict['hm_suffix']		= tuple_noun[15];
		noun_dict['kal_prefix']	  = tuple_noun[16];
		noun_dict['ha_suffix']  	= tuple_noun[17];
		noun_dict['k_suffix']  		= tuple_noun[18];
		noun_dict['annex']  			= tuple_noun[19];
		#noun_dict['definition']  	= tuple_noun[20];
		#noun_dict['note']  				= tuple_noun[21];
		# ajust some attributes
		if 		noun_dict['mamnou3_sarf']==u"":
			noun_dict['mamnou3_sarf']=u"Tn";
		if 		noun_dict['number']==u"مفرد" and 	noun_dict['feminable']=="Ta":
			noun_dict['number']=u"SnT";
		if 		noun_dict['annex']==u"Cv":
				if noun_dict['annex']=="Pm":
					noun_dict['extra1']=u"PmCv";
				elif noun_dict['annex']=="Pm":
					noun_dict['extra1']=u"PmCv";
				elif noun_dict['annex']=="Pm":
					noun_dict['extra1']=u"PmCv";

		# return a dictionary key: conjugated word, value: tags of affixes
		conjugatedDict=conjugate_noun(noun_dict);
		# create the flags for aspell
		flags_dict=create_flags(conjugatedDict, spellchecker);
		for key in flags_dict.keys():
			entry= u"/".join([key, flags_dict[key]])
			tag=u"st:%s po:noun"%noun_dict['unvocalized']
			print u"\t".join([entry, tag]).encode('utf8');
		# display
		#print u"\n".join(conjugatedDict.keys()).encode('utf8');


if __name__ == "__main__":
  main()







