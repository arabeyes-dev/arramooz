#!/usr/bin/python
# -*- coding=utf-8 -*-
#************************************************************************
# $Id: generatenoundict.py,v 0.7 2011/03/26 01:10:00 Taha Zerrouki $
#
# ------------
# Description: generate the noun dictionary for arramooz
#				if can generate dictionary in TXT, SQL, XML, and Python format
# ------------
#  Copyright (c) 2011, Arabtechies, Arabeyes Taha Zerrouki
#
#  This file is the main file to execute the application in the command line
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date: 2009/06/02 01:10:00 $
#  $Author: Taha Zerrouki $
#  $Revision: 0.7 $
#  $Source: arabtechies.sourceforge.net
#
#***********************************************************************/
import sys,re,string
import sys, getopt, os
from core.arabic_const import *
from core.ar_ctype import *
from core.noundict_functions import *
import pyarabic.araby as araby

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
MAX_LINES_TREATED=1100000;
Separator=",";

STAMP_pat=re.compile(u"[%s%s%s%s%s%s%s%s%s]"%(araby.ALEF, araby.YEH, araby.HAMZA,  araby.ALEF_HAMZA_ABOVE, araby.WAW_HAMZA,  araby.YEH_HAMZA,   araby.WAW,  araby.ALEF_MAKSURA, araby.SHADDA),re.UNICODE)
def wordStamp(word):
	"""
	generate a stamp for a word,
	remove all letters which can change form in the word :
	- ALEF,
	- HAMZA,
	- YEH,
	- WAW,
	- ALEF_MAKSURA
	- SHADDA
	@return: stamped word
	"""
	# strip the last letter if is doubled
	if word[-1:]== word[-2:-1]:
		word=word[:-1];
	return STAMP_pat.sub('',word)
#decode tags from the noun attributes
"""
0
1
2
3 مفرد/تكسير,
4المداخل,
5المفردة,
6-التأنيث,
7-التثنية,
8-"ج. مؤ. س."
9-"ج. مذ. س."
10-المنقوص,
11-"تنوين النصب"
12-,نسب,
13-"إض. لف.",
14-ـو
15-ك,
16-كال,
17-ها,
18-هم
19,1,
20-الجمع,
21-"تنوين النصب",
22- الشرح,

"""
                                                         
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-v | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\t[-d | --display= format]\tdisplay format (txt,sql, python, xml) %s"%scriptname
	print "\t[-t | --type= wordtype]\tgive the word type(fa3il,masdar, jamid, mochabaha,moubalagha,mansoub,) %s"%scriptname
	print "\t[-l | --limit= limit_ number]\tthe limit of treated lines %s"%scriptname
	print "\r\nN.B. Details in README"
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	fname = ''
	limit = MAX_LINES_TREATED;
	wordtype="typo";
	display="txto";	
	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hv:f:d:t:l:",
                               ["help", "version", "file=","display=","type=","limit=",],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-v", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-f", "--file"):
			fname = val
		if o in ("-l", "--limit"):
			try:
				limit = int(val);
			except:
				limit=MAX_LINES_TREATED;
		else:
			limit=MAX_LINES_TREATED;
		if o in ("-d", "--display"):
			display=val;
		if o in ("-t", "--type"):
			wordtype=val;
	#print fname,limit,display, wordtype;		
	return (fname,limit,display,wordtype);


                 
def main():
	filename,limit,display_format, wordtype= grabargs()
	#print filename,limit,display_format, wordtype;
	#exit();
	try:
		fl=open(filename);
	except:
		print " Error :No such file or directory: %s" % filename
		sys.exit(0)
	#print "#",filename
	fields={};
	field_id={
		'original':0,		#	#ignored
		#'root':1,			#	# root
		'root':2,			#	# root
		'vocalized':3,		#مفرد/تكسير	# vocalized word
		'unvocalized':4,		#المداخل	# unvocalized word
		'ignored':5,		#	#ignored
		'word':6,			#المفردة	# word
		'feminable':7,		#التأنيث	#does the word accept feminin
		'dualable':8,		#التثنية	#does the word accept dual_form
		'feminin_plural':9,	#ج. مؤ. س.	#does the word accept feminin_plural,
		'masculin_plural':10,#ج. مذ. س.	#does the word accept masculin_plural,
		'mankous':11,		#المنقوص	#mankous
		'tanwin_nasb':12,	#تنوين النصب	#Tanwin Nasb
		'relative':13,		#نسب	#relative
		'annex':14,			#إض. لف.	#oral annexation  إضافة لفظية
		'w_suffix':15,		#ـو	#accept Waw suffix
		'k_suffix':16,		#ك	#accept Kaf suffix
		'kal_prefix':17,	#كال	#accept Kaf+Alef+Lam  preffix
		'ha_suffix':18,		#ها	#accept Heh+Alef suffix
		'hm_suffix':19,		#هم	#accept Heh+Meem suffix
		'ignored':20,		#	#ignored
		'plural':21,		#الجمع	#accept plural form 
		'plural_tanwin_nasb':22,		#تنوين النصب	#doew th plural form accept tawnin mansoub
		'definition':24,	#الشرح	#Definition of the word
	}
	#give the display order for text format display
	display_order=[
			#'id',
			'vocalized',
			'unvocalized',
			'wordtype',
			'root',
			'normalized',
			'stamped',
			'original',
			'mankous',
			'feminable',
			'number',
			'dualable',
			'masculin_plural',
			'feminin_plural',
			'broken_plural',
			'mamnou3_sarf',
			'relative',
			'w_suffix',
			'hm_suffix',
			'kal_prefix',
			'ha_suffix',
			'k_suffix',
			'annex',
			'definition',
			'note',
			]
	wordtype_table={
		"fa3il":u"اسم فاعل",
		"masdar":u"مصدر",
		"jamid":u"جامد",
		"maf3oul":u"اسم مفعول",		
		"mouchabbaha":u"صفة مشبهة",
		"moubalagha":u"صيغة مبالغة",
		"mansoub":u"منسوب",
		"tafdil":u"اسم تفضيل",\
		"sifates":u"صفة",		
	}
	#display_format="txt"
	if wordtype in wordtype_table.keys():
		wordtype=wordtype_table[wordtype];
	else: print "Fatal Errot : unsupported wordtype", wordtype;
		#exit();
	line=fl.readline().decode("utf8");
	text=u""
	noun_table=[];
	nb_field=2;
	while line :
		line=chomp(line)
		if not line.startswith("#"):
			liste=line.split(Separator);
			if len(liste)>=nb_field:
				noun_table.append(liste);

		line=fl.readline().decode("utf8");
	fl.close();

	#print "#", (u'\t'.join(field_id.keys())).encode('utf8');
	model=0;
	#print header 
	if display_format=="txt":
		print "#", u"\t".join(display_order).encode('utf8');
	elif display_format=="sql":
		# for slite database we must omit some features
		# the ENUM and CHARSET and AUTOINCREMENT are not supported
		print u"""CREATE TABLE  IF NOT EXISTS `nouns` (
		  `id` int(11) unique auto_increment,
		  `vocalized` varchar(30) DEFAULT NULL,
		  `unvocalized` varchar(30) DEFAULT NULL,
		  `wordtype` varchar(30) DEFAULT NULL,
		  `root` varchar(30) DEFAULT NULL,
			`normalized` varchar(30) not null,
			`stamped` varchar(30) not null,		  
		  `original` varchar(30) DEFAULT NULL,
		  `mankous` varchar(30) DEFAULT NULL,
		  `feminable` varchar(30) DEFAULT NULL,
		  `number` varchar(30) DEFAULT NULL,
		  `dualable` varchar(30) DEFAULT NULL,
		  `masculin_plural` varchar(30) DEFAULT NULL,
		  `feminin_plural` varchar(30) DEFAULT NULL,
		  `broken_plural` varchar(30) DEFAULT NULL,
		  `mamnou3_sarf` varchar(30) DEFAULT NULL,
		  `relative` varchar(30) DEFAULT NULL,
		  `w_suffix` varchar(30) DEFAULT NULL,
		  `hm_suffix` varchar(30) DEFAULT NULL,
		  `kal_prefix` varchar(30) DEFAULT NULL,
		  `ha_suffix` varchar(30) DEFAULT NULL,
		  `k_suffix` varchar(30) DEFAULT NULL,
		  `annex` varchar(30) DEFAULT NULL,
		  `definition` text,
		  `note` text
		)  DEFAULT CHARSET=utf8;""".encode('utf8')
		# print u"""CREATE TABLE  IF NOT EXISTS `nouns` (
		  # `id` int(11) unique not null,
		  # `vocalized` varchar(30) DEFAULT NULL,
		  # `unvocalized` varchar(30) DEFAULT NULL,
		  # `wordtype` varchar(30) DEFAULT NULL,
		  # `root` varchar(30) DEFAULT NULL,
			# `normalized` varchar(30) not null,
			# `stamped` varchar(30) not null,		  
		  # `original` varchar(30) DEFAULT NULL,
		  # `mankous` varchar(30) DEFAULT NULL,
		  # `feminable` varchar(30) DEFAULT NULL,
		  # `number` varchar(30) DEFAULT NULL,
		  # `dualable` varchar(30) DEFAULT NULL,
		  # `masculin_plural` varchar(30) DEFAULT NULL,
		  # `feminin_plural` varchar(30) DEFAULT NULL,
		  # `broken_plural` varchar(30) DEFAULT NULL,
		  # `mamnou3_sarf` varchar(30) DEFAULT NULL,
		  # `relative` varchar(30) DEFAULT NULL,
		  # `w_suffix` varchar(30) DEFAULT NULL,
		  # `hm_suffix` varchar(30) DEFAULT NULL,
		  # `kal_prefix` varchar(30) DEFAULT NULL,
		  # `ha_suffix` varchar(30) DEFAULT NULL,
		  # `k_suffix` varchar(30) DEFAULT NULL,
		  # `annex` varchar(30) DEFAULT NULL,
		  # `definition` text,
		  # `note` text
		# );""".encode('utf8')		
	elif display_format=="xml": 
		print "<?xml version='1.0' encoding='utf-8'?>\n<dictionary>";
	elif display_format=="python": 
		print u"""#!/usr/bin/python
# -*- coding=utf-8 -*-"""

		line=u"NOUN_DICTIONATY_INDEX={";
		for k in range(len(display_order)):
			key=display_order[k];
			line+=u"u'%s':%s, "%(key,str(k));
		line+=u"}";
		print line.encode('utf8');
		print u"NOUN_DICTIONARY={".encode('utf8')
	id=1;
	for tuple_noun in noun_table[:limit]:
		#print (u'\t'.join(tuple_noun)).encode('utf8');
		#extract field from the noun tuple
		fields={};
		for key in field_id.keys():
			fields[key]=tuple_noun[field_id[key]].strip();
		# treat specific fields
		fields['note']="";
		fields['id']=id;
			# word root
		fields['root']=decode_root(fields['root']);
		if fields['root']=="":
			fields['number']=u"جمع تكسير"
			fields['note']=u":".join([fields['note'],u"لا جذر", u"لا مفرد"]);			
		else:
			fields['number']=u"مفرد"
		# original verb
		if fields['original']!="":
			fields['original']=get_original(fields['original']);

		# vocalized word
		if fields['vocalized']=="":
			fields['vocalized']=fields['unvocalized'];
			fields['note']=u":".join([fields['note'],u"لا تشكيل"]);

			# make note  if definition is not given
		if fields['definition']=="":
			fields['note']=u":".join([fields['note'],u"لا شرح"]);

		#الممنوع من الصرف
		if fields['tanwin_nasb']=="":
			fields['mamnou3_sarf']=u"ممنوع من الصرف";
		else:
			fields['mamnou3_sarf']=u"";
		# get the vocalized form 
		fields['vocalized']=decode_vocalized(fields['vocalized']);
		fields['unvocalized']=ar_strip_marks(fields['vocalized']);
		
		fields['normalized']=araby.normalizeHamza(fields['unvocalized']);
		fields['stamped']=wordStamp(fields['unvocalized']);		
		# word type, must be defined for every file			
		fields['wordtype']=wordtype;
		# create mankous form if exist
		if fields['mankous']=="Tk":
			fields['mankous_from']=get_mankous(fields['vocalized']);	
		# create feminin form if is possibel
		if fields['feminable']=="Ta":
			fields['feminin_from']=get_feminin(fields['vocalized']);	
		# extarct broken plurals
		if fields['plural']!="":
			fields['broken_plural']=get_broken_plural(fields['plural']);	
		else:
			fields['broken_plural']="";
		#display order

		if display_format=="txt":
			items=[];
			for k in range(len(display_order)):
				key=display_order[k];
				items.append(fields[key]);
				line=u"\t".join(items);
		elif display_format=="sql": 
			# to reduce the sql file size, 
			# doesn't work with multiple files
			#items=["'%d'"%id,];
			#line="insert into nouns values "   #%", ".join(display_order);
			line="insert into nouns (%s) values "%", ".join(display_order);
			
			items=[];			
			for k in range(len(display_order)):
				key=display_order[k];
				items.append(u"'%s'"%fields[key]);
			line+=u"(%s);"%u",".join(items);
		elif display_format=="xml": 
			line="<noun>";
			for k in range(len(display_order)):
				key=display_order[k];
				line+=u"<%s>%s</%s> "%(key,fields[key],key);
			line+=u"</noun>";
		elif display_format=="python": 
			line="u'%s':{"%fields['vocalized'];
			for k in range(len(display_order)):
				key=display_order[k];
				line+=u"%s:u'%s', "%(str(k),fields[key]);
			line+=u"},";

		
		else: line="unsupported format";
		print line.encode('utf8');
		#increment ID
		id+=1;
	# end tags
	
	if display_format=="xml": 
		print "</dictionary>";
	if display_format=="python": 
		print "};";

if __name__ == "__main__":
  main()







