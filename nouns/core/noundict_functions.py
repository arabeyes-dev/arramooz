#!/usr/bin/python
# -*- coding=utf-8 -*-
#************************************************************************
# $Id: generatenoundict.py,v 0.7 2011/03/26 01:10:00 Taha Zerrouki $
#
# ------------
# Description:
# ------------
#  Copyright (c) 2011, Arabtechies, Arabeyes Taha Zerrouki
#
#  This file is the main file to execute the application in the command line
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date: 2009/06/02 01:10:00 $
#  $Author: Taha Zerrouki $
#  $Revision: 0.7 $
#  $Source: arabtechies.sourceforge.net
#
#***********************************************************************/

from arabic_const import *
from ar_ctype import *


import sys,re,string
import sys, getopt, os
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
MAX_LINES_TREATED=1100000;
Separator=",";

# treat the root, strip extra characters
def decode_root(root):
	root=root.replace(' ','')
	root=root.replace('[','')
	root=root.replace(']','')
	root=root.replace('"','')	
	root=root.replace(TATWEEL,'')
	return root;

# treat the vocalized form, strip extra characters
def decode_vocalized(vocalized):
	vocalized=vocalized.split('-');
	vocalized=vocalized[0].strip();
	#strip plural marks by ج:
	vocalized=vocalized.replace(u'ج:','')	
	vocalized=re.sub(ur'[ ,."]','',vocalized)
	#vocalized=vocalized.replace(',','')
	return vocalized;
# create the mankous form if t's possible
def get_mankous(vocalized):
	if vocalized.endswith(YEH):
		#strip last yeh
		return vocalized[:-1]+KASRATAN;
	else:
		print (u"#*********%s"%vocalized).encode('utf8');
		#exit();
# create the mankous form if it's possible
def get_feminin(vocalized):
	if vocalized[-1:] in (DAMMA, DAMMATAN):
		#strip last yeh
		vocalized=vocalized[:-1];
	return vocalized+FATHA+TEH_MARBUTA+DAMMATAN;
# extract broken plurals from the plural fields
def get_broken_plural(plural):

	broken_plural=plural
	broken_plural=broken_plural.replace(u'ـات','');
	broken_plural=broken_plural.replace(u'ـون','');
	broken_plural=broken_plural.replace(u'ج:','');
	broken_plural=broken_plural.replace(u'.','');	
	broken_plural=broken_plural.replace(u':','');		
	brokens=broken_plural.split(u'،');
	brokensvalid=[];
	for b in brokens:
		b=b.replace(' ','');
		if b!=u"":
			brokensvalid.append(b);
	brokens=u":".join(brokensvalid);
	return brokens;

def get_original(original):
	"""Extract the original word from the field"""
	original=original.replace(u')','');
	original=original.replace(u'(','');	
	original=original.replace(u'"','');		
	return original;

	