#!/usr/bin/python
# -*- coding=utf-8 -*-
#
import re, string,sys
from core.arabic_const import *
from core.ar_ctype import *
from core.spelltools import *

def chomp(s):
  if (s.endswith('\n')):
    return s[:-1]
  else:
    return s

#-----------------------------------------------
#
#
#-----------------------------------------------
pronouns=(
KASRA+YEH,
FATHA+HEH+DAMMA,
DAMMA+HEH+DAMMA,
KASRA+HEH+KASRA,
);
FLAGS_TABLE={
0: #not defined
	{0:  # not majrour
		{0:u"ngnr",# not annex
		 1:u"ngnrcc",# annex
		},
	1:# majrour
		{0:u"ngnjnl",# not annex
		 1:u"ngnjnlcc",# annex
		},
	},
1:#defined
	{0:# not majrour
		{0:u"ngnr",# not annex
		 1:u"ngnr",# annex
		},
	1:# majrour
		{0:u"ngnjmj",# not annex
		 1:u"ngnjmj",# annex
		},
	},
}

FLAGS_TABLE_ASPELL={
0: #not defined
	{0:  # not majrour
		{0:u"ZY",# not annex
		 1:u"ZYz",# annex
		},
	1:# majrour
		{0:u"ZXW",# not annex
		 1:u"ZXWz",# annex
		},
	},
1:#defined
	{0:# not majrour
		{0:u"ZY",# not annex
		 1:u"ZY",# annex
		},
	1:# majrour
		{0:u"ZXV",# not annex
		 1:u"ZXV",# annex
		},
	},
}


CONJ_SUFFIX_LIST_TAGS={
#u"":{'tags':(u'',u'مجرور'),'vocalized':(DAMMA,KASRA,FATHA,DAMMATAN,KASRATAN)},
u"ُ":{'tags':(u'مرفوع', u'يضاف'), 'vocalized':(u"ُ",)},#TEH_MARBUTA,
u"ٌ":{'tags':(u'مرفوع',u"تنوين"), 'vocalized':(u"ٌ",)},#TEH_MARBUTA,
u'ٍ':{'tags':(u'مجرور', u"تنوين"), 'vocalized':(u'ٍ',)},#TEH_MARBUTA,
u'ِ':{'tags':(u'مجرور', u'يضاف'), 'vocalized':(u'ِ',)},#TEH_MARBUTA,
u'َ':{'tags':(u'منصوب', u'يضاف'), 'vocalized':(u'َ',)},#TEH_MARBUTA,
#u"ً":{'tags':(u'منصوب', u"تنوين"), 'vocalized':(u"ً",)},#TEH_MARBUTA,

#u"ة":{'tags':(u'مؤنث',u'مجرور'),'vocalized':(u"َةُ",u"َةٌ",u'َةٍ',u'َةِ',u'َةَ',u"َةً")},#TEH_MARBUTA,
u"َةُ":{'tags':(u'مؤنث',u'مرفوع', u'يضاف'), 'vocalized':(u"َةُ",), },#TEH_MARBUTA,
u"َةٌ":{'tags':(u'مؤنث',u'مرفوع',u"تنوين", ), 'vocalized':(u"َةٌ",)},#TEH_MARBUTA,
u'َةٍ':{'tags':(u'مؤنث',u'مجرور', u"تنوين", ), 'vocalized':(u'َةٍ',)},#TEH_MARBUTA,
u'َةِ':{'tags':(u'مؤنث',u'مجرور', u'يضاف', ), 'vocalized':(u'َةِ',)},#TEH_MARBUTA,
u'َةَ':{'tags':(u'مؤنث',u'منصوب', u'يضاف', ), 'vocalized':(u'َةَ',)},#TEH_MARBUTA,
u"َةً":{'tags':(u'مؤنث',u'منصوب', u"تنوين", ), 'vocalized':(u"َةً",)},#TEH_MARBUTA,

#u"ية":{'tags':(u'مؤنث',u'مجرور'),'vocalized':(u"ِيَّةُ",u"ِيَّةٌ",u'ِيَّةٍ',u'ِيَّةِ',u'ِيَّةَ',u"ِيَّةً")},#TEH_MARBUTA,
u"ِيَّةُ":{'tags':(u'مؤنث',u'مرفوع', u'يضاف', u'نسبة'), 'vocalized':(u"ِيَّةُ",)},#TEH_MARBUTA,
u"ِيَّةٌ":{'tags':(u'مؤنث',u'مرفوع',u"تنوين", u'نسبة'), 'vocalized':(u"ِيَّةٌ",)},#TEH_MARBUTA,
u'ِيَّةٍ':{'tags':(u'مؤنث',u'مجرور', u"تنوين", u'نسبة'), 'vocalized':(u'ِيَّةٍ',)},#TEH_MARBUTA,
u'ِيَّةِ':{'tags':(u'مؤنث',u'مجرور', u'يضاف', u'نسبة'), 'vocalized':(u'ِيَّةِ',)},#TEH_MARBUTA,
u'ِيَّةَ':{'tags':(u'مؤنث',u'منصوب', u'يضاف', u'نسبة'), 'vocalized':(u'ِيَّةَ',)},#TEH_MARBUTA,
u"ِيَّةً":{'tags':(u'مؤنث',u'منصوب', u"تنوين", u'نسبة'), 'vocalized':(u"ِيَّةً",)},#TEH_MARBUTA,

#u"ا":{'tags':(u'مثنى',u'مضاف',),'vocalized':(u'َا',u'ًا')},#ALEF,
u"َا":{'tags':(u'مثنى',u'مضاف',),'vocalized':(u'َا',)},#ALEF,
u"ًا":{'tags':(u'تنوين',),'vocalized':(u'ًا',)},#ALEF,

#u"ت":{'tags':(u'مؤنث',u'مضاف',u'مجرور'),'vocalized':(u'َت',)},#TEH_MARBUTA,
#u"يت":{'tags':(u'مضاف',u'مجرور'),'vocalized':(u'َيَّتَ',)},#YEH+TEH_MARBUTA,
#u"ات":{'tags':(u'جمع مؤنث سالم',u'مجرور',u'جمع'),'vocalized':(u'َاتُ',u'َاتٌ',u'َاتٍ',u'َاتِ')},#ALEF+TEH,
u'َاتُ':{'tags':(u'جمع مؤنث سالم',u'مرفوع',u'جمع', u'يضاف'), 'vocalized':(u'َاتُ',)},#ALEF+TEH,

u'َاتٌ':{'tags':(u'جمع مؤنث سالم',u'مرفوع',u'تنوين',u'جمع'), 'vocalized':(u'َاتٌ',)},#ALEF+TEH,
u'َاتٍ':{'tags':(u'جمع مؤنث سالم',u'مجرور',u'منصوب',u'تنوين',u'جمع'), 'vocalized':(u'َاتٍ',)},#ALEF+TEH,
u'َاتِ':{'tags':(u'جمع مؤنث سالم',u'مجرور',u'منصوب',u'جمع', u'يضاف'), 'vocalized':(u'َاتِ',)},#ALEF+TEH,

#u"ون":{'tags':(u'جمع مذكر سالم',u'لايضاف',u'جمع'),'vocalized':(u'ُونَ',)},#WAW+NOON,
u'ُونَ':{'tags':(u'جمع مذكر سالم',u'لايضاف',u'جمع'),'vocalized':(u'ُونَ',)},#WAW+NOON,

#u"ين":{'tags':(u'جمع مذكر سالم',u'مجرور',u'لايضاف',u'جمع'),'vocalized':(u'ِينَ', u'َيْنِ')},#YEH+NOON,
u'ِينَ':{'tags':(u'جمع مذكر سالم',u'مجرور',u'لايضاف',u'جمع'),'vocalized':(u'ِينَ',)},#YEH+NOON,
u'َيْنِ':{'tags':(u'جمع مذكر سالم',u'منصوب',u'لايضاف',u'جمع'),'vocalized':(u'َيْنِ',)},#YEH+NOON,

#u"و":{'tags':(u'جمع مذكر سالم',u'مضاف',u'جمع'),'vocalized':(u'ُو',)},#WAW,
u'ُو':{'tags':(u'جمع مذكر سالم',u'مضاف',u'جمع'),'vocalized':(u'ُو',)},#WAW,
#u"ي":{'tags':(u'جمع مذكر سالم',u'مضاف',u'مجرور',u'جمع'),'vocalized':(u'ِيِ',)},#YEH,
u'ِيِ':{'tags':(u'جمع مذكر سالم',u'مضاف',u'مجرور',u'جمع'),'vocalized':(u'ِيِ',u'َيْ')},#YEH,
u'َيِْ':{'tags':(u'جمع مذكر سالم',u'مضاف',u'منصوب',u'جمع'),'vocalized':(u'ِيِ',)},#YEH,


#u"ان":{'tags':(u'مثنى',u'لايضاف'),'vocalized':(u'َانِ',)},#ALEF+NOON,
u'َانِ':{'tags':(u'مثنى',u'لايضاف'),'vocalized':(u'َانِ',)},#ALEF+NOON,
#u"تين":{'tags':(u'مثنى',u'مجرور',u'لايضاف'),'vocalized':(u'َتَيْنِ',)},#TEH+YEH+NOON,
u'َتَيْنِ':{'tags':(u'مثنى',u'مجرور',u'لايضاف'),'vocalized':(u'َتَيْنِ',)},#TEH+YEH+NOON,

# النسب
u'ِي':{'tags':(u'يضاف', u'نسبة',),'vocalized':(u'ِيِ',)},#YEH,

#u"تان":{'tags':(u'مثنى',u'لايضاف'),'vocalized':(u'َتَانِِ',)},#TEH+ALEF+NOON,
u'َتَانِِ':{'tags':(u'مثنى',u'لايضاف'),'vocalized':(u'َتَانِِ',)},#TEH+ALEF+NOON,
u"يتين":{'tags':(u'مثنى',u'مجرور',u'لايضاف', u'نسبة'),'vocalized':(u'ِيَّتَيْنِ',)},#TEH+YEH+NOON,
u'ِيَّتَيْنِ':{'tags':(u'مثنى',u'مجرور',u'لايضاف',u'نسبة'),'vocalized':(u'ِيَّتَيْنِ',)},#TEH+YEH+NOON,
#u"يتان":{'tags':(u'مثنى',u'لايضاف'),'vocalized':(u'ِيَّتَانِِ',)},#TEH+ALEF+NOON,
u'ِيَّتَانِِ':{'tags':(u'مثنى',u'لايضاف',u'نسبة'),'vocalized':(u'ِيَّتَانِِ',)},#TEH+ALEF+NOON,
#u"يات":{'tags':(u'جمع مؤنث سالم',u'مجرور',u'جمع'),'vocalized':(u'ِيَاتِ',u'ِيَاتُ',u'ِيَاتٌ',u'ِيَاتٍ',)},#YEH+ALEF+TEH,
u'ِيَاتُ':{'tags':(u'جمع مؤنث سالم',u'مرفوع',u'جمع',u'يضاف', u'نسبة'), 'vocalized':(u'ِيَاتُ',)},#YEH+ALEF+TEH,
u'ِيَاتٌ':{'tags':(u'جمع مؤنث سالم',u'مرفوع',u'تنوين',u'جمع', u'نسبة'),'vocalized':(u'ِيَاتٌ',)},#YEH+ALEF+TEH,
u'ِيَاتِ':{'tags':(u'جمع مؤنث سالم', u'مجرور', u'منصوب', u'جمع', u'يضاف', u'نسبة'), 'vocalized': (u'ِيَاتِ', )},#YEH+ALEF+TEH,
u'ِيَاتٍ':{'tags':(u'جمع مؤنث سالم',u'مجرور', u'منصوب', u'تنوين', u'جمع', u'يضاف', u'نسبة'), 'vocalized':(u'ِيَاتٍ',)},#YEH+ALEF+TEH,
};


#----------------------------------------------
# give all compatible suffixes with noun attributs given in dictionary
#----------------------------

def compatible_suffixes_dictionary(noun_dict):

	suffix_list=CONJ_SUFFIX_LIST_TAGS.keys();
	#print u"\t".join(CONJ_SUFFIX_LIST_TAGS.keys()).encode('utf8');	
	tmp_list=[]+suffix_list;
	for suffix in suffix_list:
	# استبعاد كل حالات التنوين إذا كان ممنوعا من الصرف
		if u"تنوين" in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and u"ممنوع من الصرف" in noun_dict.values():
			tmp_list.remove(suffix);
	#print ":".join(tmp_list).encode('utf8');
	#print ":".join(suffix_list).encode('utf8');
	#return suffix_list;
	suffix_list=[]+tmp_list
	for suffix in suffix_list:
	# استبعاد كل حالات جمع المذكر السالم
		if u"جمع مذكر سالم" in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and "Pm" not in noun_dict.values():
			tmp_list.remove(suffix);
	suffix_list=[]+tmp_list
	for suffix in suffix_list:
	# استبعاد كل حالات جمع المؤنث السالم
		if u"جمع مؤنث سالم" in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and "Pf" not in noun_dict.values():
			tmp_list.remove(suffix);
	suffix_list=[]+tmp_list
	for suffix in suffix_list:
# استبعاد كل حالات المثنى
		if u"مثنى" in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and "Dn" not in noun_dict.values():
			tmp_list.remove(suffix);
	suffix_list=[]+tmp_list	


	for suffix in suffix_list:
# استبعاد كل حالات المؤنث
		if u'مؤنث' in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and "Ta" not in noun_dict.values():
			tmp_list.remove(suffix);
	suffix_list=[]+tmp_list
	for suffix in suffix_list:
		if u'نسبة' in CONJ_SUFFIX_LIST_TAGS[suffix]["tags"] and "R" not in noun_dict.values():				
			tmp_list.remove(suffix);
	suffix_list=[]+tmp_list
	#print u"\t".join(CONJ_SUFFIX_LIST_TAGS.keys()).encode('utf8');	
	#print u"\t".join(noun_dict.values()).encode('utf8');		

	#print u"\t".join(suffix_list).encode('utf8');			
	return suffix_list;


# generate a word stem according to the suffix
def get_word_stem(word,suffix):
	word_stem=word;
	HARAKAT=(FATHA,DAMMA,KASRA,SUKUN, DAMMA, DAMMATAN, KASRATAN, FATHATAN);
	#if the word ends by a haraka
	if word_stem[-1:] in HARAKAT:
		word_stem=word_stem[:-1]

	if word_stem.endswith(TEH_MARBUTA) and suffix not in HARAKAT:
		word_stem=word_stem[:-1]+TEH;
	elif word_stem.endswith(ALEF_MAKSURA) and suffix not in HARAKAT:
		word_stem=word_stem[:-1]+YEH;
	elif word_stem.endswith(HAMZA) and suffix not in HARAKAT:
		if suffix.startswith(DAMMA):
			word_stem=word_stem[:-1]+WAW_HAMZA;
		elif suffix.startswith(KASRA):
			word_stem=word_stem[:-1]+YEH_HAMZA;
			
	return word_stem;


def generate_allforms(noun_dict):

	has_definition=True;
	has_pronouns=True;
	word=noun_dict['vocalized'];

	mylist=[word];
	tags={};
	tags[word]=(u'مرفوع',);
	temp=[];
	list_possible_suffixes=compatible_suffixes_dictionary(noun_dict);
	for suffix in list_possible_suffixes:
			#treat the word
			word_stem=get_word_stem(word,suffix);
			newword="-".join([word_stem,suffix]);				
			temp.append(newword);
			tags[newword]=CONJ_SUFFIX_LIST_TAGS[suffix]['tags']
	

# affixatition section				
	mylist+=temp;
	temp=[];
	#has_pronouns=False;
	if has_pronouns:
		# add pronoun only for basic word
			for p in pronouns:
				word_stem=get_word_stem(word,p);
				newword="-".join([word_stem,p]);
				temp.append(newword);
				tags[newword]=(u'مضاف إليه',);
		
	if has_definition:
		for w in mylist:
			prefix=ALEF+LAM
			if u'مضاف' not in tags[w] and u'تنوين' not in tags[w]:
					newword="-".join([prefix,w]);
					temp.append(newword);
					tags[newword]=tags[w]+(u'معرف',);
	mylist+=temp;
	#print mylist;
	return tags;

def create_flags(tags_dic,spellchecker):
	flags_dic={};
	unvo_flags_dic={};
	if spellchecker=="aspell":
		table_flags=FLAGS_TABLE_ASPELL;
	else:
		table_flags=FLAGS_TABLE;
	for key in tags_dic.keys():
		if u"معرف" in tags_dic[key]:
			defined=1;
		else: defined=0;
		if u"مجروز" in tags_dic[key]:
			majror=1;
		else: majror=0;
		if u"مضاف" in tags_dic[key] or  u"يضاف" in tags_dic[key]:
			annex=1;
		else: annex=0;
		flags_dic[key]=table_flags[defined][majror][annex];

		# reduce word entree by removing harakat
		key_nm=ar_strip_marks(key);
		if unvo_flags_dic.has_key(key_nm): unvo_flags_dic[key_nm]+=flags_dic[key];
		else:
			unvo_flags_dic[key_nm]=flags_dic[key];
	#for key in flags_dic.keys():
	#	print u"/".join([key, flags_dic[key]]).encode('utf8');
	for key_nm in unvo_flags_dic.keys():
		unvo_flags_dic[key_nm]= unify_flags(unvo_flags_dic[key_nm])
	return  unvo_flags_dic;

#-----------------
# deprecated, used only to remove dashes
#-----------------
def standardize_form(word,vocalized=False):
	word=re.sub(u"-",'', word);
	return word;


def conjugate_noun(noun_dict):
				  
	dict0=generate_allforms(noun_dict);
	dict1={}
	for k in dict0.keys():
		standard=standardize_form(k,True);
		if dict1.has_key(standard):
			dict1[standard]+=dict0[k];
		else:
			dict1[standard]=dict0[k];
	return 		dict1;

