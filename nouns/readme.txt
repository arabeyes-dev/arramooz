#-----------
# Noun dictionary generation for Arramooz
#-------------
Arramooz is an arabic open source dictionary, built from ayaspell project,
its special for morphology analysis.


# FILES
========
* data/ contains the brut data from the ayaspell data
* core/ special functions
* generatenoundict.py : the main sciprt, usage is descripted in USAGE section.
* gennoundict_sql.bat : a shell script to create a sql format of the dictionary
* gennoundict_txt.bat : a shell script to create a txt format of the dictionary
* gennoundict_xml.bat : a shell script to create a xml format of the dictionary
* gennoundict_py.bat : a shell script to create a python format of the dictionary

# USAGE
========
generatenoundict.py
(C) CopyLeft 2009, Taha Zerrouki
Usage: generatenoundict -f filename [OPTIONS]
        [-h | --help]           outputs this usage message
        [-v | --version]        program version
        [-f | --file= filename] input file to generatenoundict
        [-d | --display= format]        display format (txt,sql, python, xml) ge
neratenoundict
        [-t | --type= wordtype] give the word type(fa3il,masdar, jamid, mochabah
a,moubalagha,mansoub,) generatenoundict
        [-l | --limit= limit_ number]   the limit of treated lines generatenound
ict


This program is licensed under the GPL License
