﻿# create a dictionary file from ayaspell cvs form
# fa3il file
generatenoundict.py  -f data/fa3il.csv  -t fa3il -d xml  >output/nouns.fa3il.dict.xml
generatenoundict.py  -f data/maf3oul.csv  -d xml -t maf3oul >output/nouns.maf3oul.dict.xml
generatenoundict.py  -f data/jamid.csv  -d xml -t jamid >output/nouns.jamid.dict.xml
generatenoundict.py  -f data/mansoub.csv -d xml -t mansoub >output/nouns.mansoub.dict.xml
generatenoundict.py  -f data/masdar.csv  -d xml -t masdar >output/nouns.masdar.dict.xml
generatenoundict.py  -f data/moubalagha.csv  -d xml -t moubalagha >output/nouns.moubalagha.dict.xml
generatenoundict.py  -f data/mouchabbaha.csv -d xml -t mouchabbaha >output/nouns.mouchabbaha.dict.xml
generatenoundict.py -f data/sifates.csv  -d xml -t sifates  >output/nouns.sifates.dict.xml
generatenoundict.py -f data/tafdil.csv   -d xml -t tafdil >output/nouns.tafdil.dict.xml
