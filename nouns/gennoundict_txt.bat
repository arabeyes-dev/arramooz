﻿# create a dictionary file from ayaspell cvs form
# fa3il file
python generatenoundict.py  -f data/fa3il.csv -d txt -t fa3il  >output/nouns.dict.txt
## maf3oul file
python generatenoundict.py  -f data/maf3oul.csv -d txt -t maf3oul >>output/nouns.dict.txt

## jamid file
python generatenoundict.py  -f data/jamid.csv -d txt -t jamid >>output/nouns.dict.txt

## mansoub.csv
python generatenoundict.py  -f data/mansoub.csv -d txt -t mansoub >>output/nouns.dict.txt

## masdar.csv
python generatenoundict.py  -f data/masdar.csv -d txt -t masdar >>output/nouns.dict.txt

## moubalagha.csv
python generatenoundict.py  -f data/moubalagha.csv -d txt -t moubalagha >>output/nouns.dict.txt

## mouchabbaha.csv
python generatenoundict.py  -f data/mouchabbaha.csv -d txt -t mouchabbaha >>output/nouns.dict.txt

## sifates.csv
python generatenoundict.py -f data/sifates.csv -d txt -t sifates  >>output/nouns.dict.txt

## tafdil.csv
python generatenoundict.py -f data/tafdil.csv  -d txt -t tafdil >>output/nouns.dict.txt
