﻿create table verbs
(
id int unique auto_increment,
vocalized varchar(30) not null,
unvocalized varchar(30) not null,
root varchar(30),
future_type varchar(5),
triliteral  ENUM( "n", "y" ) NOT NULL default "y", 
transitive  ENUM( "n", "y" ) NOT NULL default "y", 
double_trans  ENUM( "n", "y" ) NOT NULL default "y", 
think_trans  ENUM( "n", "y" ) NOT NULL default "y", 
unthink_trans  ENUM( "n", "y" ) NOT NULL default "y", 
reflexive_trans  ENUM( "n", "y" ) NOT NULL default "y", 
past  ENUM( "n", "y" ) NOT NULL default "y", 
future  ENUM( "n", "y" ) NOT NULL default "y",  
imperative  ENUM( "n", "y" ) NOT NULL default "y", 
passive  ENUM( "n", "y" ) NOT NULL default "y",  
future_moode  ENUM( "n", "y" ) NOT NULL default "y", 
confirmed  ENUM( "n", "y" ) NOT NULL default "y", 
PRIMARY KEY (id)
)


