#! /usr/bin/python
from distutils.core import setup
from glob import glob

# to install type:
# python setup.py install --root=/

setup (name='Arramooz Arabic Dictionary', version='0.4',
      description='Arramooz Arabic Dictionary',
      author='Taha Zerrouki',
      author_email='taha_zerrouki@gawab.com',
      url='http://Arramooz Arabic Dictionary.sourceforge.net/',
      license='GPL',
      #Description="Arabic Arabic text tools for Python",
      #Platform="OS independent",
      package_dir={'Arramooz': 'arramooz',},
      packages=['arramooz'],
      # include_package_data=True,
      package_data = {
        'Arramooz Arabic Dictionary': ['data/*.*','doc/html/*','doc/*.*',],
        },
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: End Users/Desktop',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          ],
    );

